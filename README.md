# Dataset

Dataset of spatial data.

## Sources

### [NASA Earth Observations (NEO)](https://neo.sci.gsfc.nasa.gov/)
License:
    The images available in NEO are freely available for public use without further permission. Please use the credit statement attached to each dataset, or at the very least credit NASA Earth Observations as the source.

#### NEO global datasets available as floating point GeoTIFFs:
Data index:
https://neo.sci.gsfc.nasa.gov/dataset_index.php

Description:
ftp://neoftp.sci.gsfc.nasa.gov/geotiff.float/README.txt

Datasets address:
ftp://neoftp.sci.gsfc.nasa.gov/geotiff.float/

To get all files from a directory, for example:  Active Fires (1 month - Terra/MODIS) from *MOD14A1_M_FIRE* directory.

Long command:
wget --continue --recursive --accept=.TIFF ftp://neoftp.sci.gsfc.nasa.gov/geotiff.float/MOD14A1_M_FIRE/

Short command:
wget -c -r -A .TIFF ftp://neoftp.sci.gsfc.nasa.gov/geotiff.float/MOD14A1_M_FIRE/

### [POWER Project Data Sets](https://power.larc.nasa.gov/)
Solar and meteorological data sets from NASA research for support of renewable energy, building energy efficiency and agricultural needs.

#### Viewer
https://power.larc.nasa.gov/data-access-viewer/